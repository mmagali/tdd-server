'use strict';

const mongoose = require('mongoose');

const shareRequestSchema = new mongoose.Schema(
	{
		guestEmail: String,
		hostEmail: String,
		canShare: Boolean,
		fileId: mongoose.Schema.Types.ObjectId,
		accepted: {
			type: Boolean,
			required: true,
			default: false
		}
	}
);

module.exports = shareRequestSchema;
